package ru.tsc.fuksina.tm.api;

import ru.tsc.fuksina.tm.model.Command;

public interface ICommandRepository {
    Command[] getTerminalCommands();
}
